#include <malloc.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

struct vector;

struct maybe_vector {
	bool valid;
	struct vector* vec;
};

struct maybe_int64 {
	bool valid;
	int64_t val;
};

struct maybe_vector vector_create(size_t sz);

void vector_destroy(struct vector *v);

struct maybe_int64 vector_at(const struct vector* v, size_t i);

void vector_push_back(struct vector* v, int64_t val);

bool vector_rewrite_at(struct vector* v, size_t i, int64_t val);

void vector_print(FILE *f, const struct vector* v);

struct maybe_int64 vector_lenght(const struct vector* v);
