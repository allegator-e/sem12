#include "vector.h"

#define default_capacity 16

struct vector {
	int64_t *array;
	size_t capacity;
	size_t count;
};

static struct maybe_vector some_vector(struct vector* v) {
	return (struct maybe_vector) {.valid = true, .vec = v};
}

const static struct maybe_vector none_vector = {false, NULL};

static struct maybe_int64 some_int64(int64_t v) {
	return (struct maybe_int64) {.valid = true, .val = v};
}

const static struct maybe_int64 none_int64 = {false, 0};

struct maybe_vector vector_create(size_t sz) {
    if (sz > 0) {
	struct vector* v = malloc(sizeof(struct vector));
	v->count = 0;
	v->capacity = sz;
	v->array = malloc(sizeof(int64_t) * sz);
	return some_vector(v);
    } else
        return none_vector;
}

void vector_destroy(struct vector *v) {
	free(v->array);
	free(v);
}

struct maybe_int64 vector_at(const struct vector* v, size_t i) {
	if (v != NULL && 0 <= i && i <= v->count)
		return some_int64( (v->array)[i] );
	else
		return none_int64;
}

static void vector_realloc(struct vector* v) {
	v->capacity = v->capacity * 2;
	v->array = realloc(v->array, sizeof(int64_t) * v->capacity);
}

static void count_inc(struct vector* v) {
	(v->count)++;
	if (v->count == v->capacity) {
		vector_realloc(v);
	} 
}

void vector_push_back(struct vector* v, int64_t val) {
	if (v == NULL)
		v = vector_create( default_capacity ).vec;
	(v->array)[v->count] = val;
	count_inc(v);
}

bool vector_rewrite_at(struct vector* v, size_t i, int64_t val) {
	if (v != NULL && 0 <= i && i <= v->count) {
		(v->array)[i] = val;
		return true;
	}
	return false;
}

static void print(FILE *f, int64_t val) {
	fprintf(f, "%" PRId64 " ", val);
}

void vector_print(FILE *f, const struct vector* v) {
	if (v != NULL) {
		for (size_t i = 0; i < v->count; i++) {
			print(f, v->array[i]);
		}
	}
}

struct maybe_int64 vector_lenght(const struct vector* v) {
	if (v != NULL)
		return some_int64( v->count );
	else
		return none_int64;
}
